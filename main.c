/*
 * Project: RC Switch
 * by Jonghoe Koo (AudioCookie, idnine@gmail.com)
 * ATtiny45 / Internal 8MHz
 * Datasheet: doc2586.pdf
 *
 */

// =============================================================
// Hardware Config
// pin 8 : VCC
// pin 7 : PB2 : PWM Servo Signal IN
// pin 4 : GND
// pin 3 : PB4 : LED Active Low, SERVO Signal Result
// pin 2 : PB3 : LED Active Low, Main Program Loop Active Blink
// pin 1 : RESET : Pull-up 10k
// =============================================================

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define		THRESHOLD		15

#define		SERVO_DDR		DDRB
#define		SERVO_PORT		PORTB
#define		SERVO_PIN		PINB
#define		SERVO			PB2

#define		LED_DDR			DDRB
#define		LED_PORT		PORTB
#define		LED_TOGG		PB3
#define		LED_TACT		PB4

#define		TACT_DELAY		5

volatile	int oldValue = 0;
volatile	int isChange = 0;

void hardwareInit(void)
{
	LED_DDR |= (1<<LED_TOGG) | (1<<LED_TACT);		// LED Output
	LED_PORT |= (1<<LED_TOGG) | (1<<LED_TACT);		// LED Default OFF, HIGH 
	SERVO_PORT |= (1<<SERVO);						// Servo Signal Input Pull-Up
	PCMSK |= (1<<PCINT0); 							// Pin Change Enable Mask 0
	MCUCR |= (1<<ISC01) | (1<<ISC00);				// rising edge of INT0
	GIMSK |= (1<<INT0);								// Pin Change Interrupt Enable
}

ISR(INT0_vect)
{
	int INT_CNT = 0;
	int newValue = 0;
	int i;
	
	//
	// SIGNAL HIGH COUNT
	//
	for(i=0; i<20; i++)
	{
		if(bit_is_set(SERVO_PIN, SERVO))
		{
			INT_CNT++;
		}
		_delay_us(100);
	}
	//
	// TOGGLE SWITCH
	//
	if(INT_CNT >= THRESHOLD)
	{
		LED_PORT |= (1<<LED_TOGG);
		newValue = 1;
	}
	else
	{
		LED_PORT &= ~(1<<LED_TOGG);
		newValue = 0;
	}
	//
	// TACT SWITCH
	//
	if(newValue == oldValue)
	{
		if(isChange > TACT_DELAY)
		{
			LED_PORT &= ~(1<<LED_TACT);
			isChange = 0;
		}
		isChange++;
	}
	else
	{
		oldValue = newValue;
		isChange = 1;
		LED_PORT |= (1<<LED_TACT);
	}
}

int main(void)
{
	hardwareInit();
	sei();
	while(1)
	{
		// Nothing To Do
	}
}
